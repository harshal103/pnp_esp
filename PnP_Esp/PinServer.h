
void handleRootPost() {
  /*
     TODO:
      Manipulate json and control appliance
      respond with ok.
  */

  // get payLoad data from local client
  String pin = server.arg("pin");
  String notify = server.arg("notify");
  String state = server.arg("state");

  //try to create json object
  DynamicJsonBuffer buffer(JSON_PARSER_SIZE_SINGLE_PIN);
  JsonObject& pinObject = buffer.createObject();
  pinObject["pin"] = pin;
  pinObject["notify"] = notify;
  pinObject["state"] = state;

  //send json to arduino
  pinObject.printTo(Serial);
  Serial.println();
  server.send(200, "application/json", "{\"ok\":true}");

  /*
    String payLoadFromLocal = server.arg("data");
    if(DEBUG)
    Serial.println(payLoadFromLocal);
    // try to parse json
    DynamicJsonBuffer buffer(4 * JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(4));
    JsonObject& root = buffer.parseObject(payLoadFromLocal);

    // if parsing fails, return
    if (!root.success()) {
    if (DEBUG)
      Serial.println("Json parsing failed! PinServer.h handleRootPost()");
    return;
    }

    // scan for all 4 pins and if changed, send to arduino
    for (int i = 0; i < 4; i++)
    {
    JsonObject& pinObject = root["pin" + String(i)];
    if (pinObject["changed"].as<String>() == "true" ){
      root.printTo(Serial);
      return;
    }

    }
    return;
  */
}
void handleRootGet() {
  /*
     TODO:
      ask arduino for pin status
      respond with json of all pin's states
  */

  // ask arduino for status
  Serial.println("{\"request\": \"true\"}");

  // wait till arduino responds...
  while (!Serial.available());
  String payLoadFromArduino = Serial.readString();

  // try to parse arduino string as a json object
  DynamicJsonBuffer buffer(JSON_PARSER_SIZE_SINGLE_PIN);
  JsonObject& root = buffer.parseObject(payLoadFromArduino);

  if (!root.success()) {
    if (DEBUG)
      Serial.println("Conversion from string to json object failed!");
    return;
  }
  //send response with json object as a string

  root.printTo(payLoadFromArduino);

  server.send(200, "application/json", payLoadFromArduino);
  return;
}
void setupServer() {
  server.on("/", HTTP_GET, handleRootGet);
  server.on("/", HTTP_POST, handleRootPost);
  server.begin();

  if (DEBUG)
    Serial.println("it should handle clients now!");
}
void stopServer() {
  server.stop();
  server.close();
}
