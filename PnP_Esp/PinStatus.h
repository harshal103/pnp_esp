#include "ESP8266HTTPClient.h"
#include "ArduinoJson.h"

void resetNotify(int pin,int state){
  String payLoadToSend="pin_no="+String(pin)+"&pin_state="+String(state)+"&device_mac=" + WiFi.macAddress()+"&notify_controller=0";
  http.begin(PIN_STATUS_POST_URL);
  http.addHeader("Content-Type","application/x-www-form-urlencoded");
  http.POST(payLoadToSend);
  
}

void getPinStatus() {
  /*
     call setTimeout before connection to influence connection timeout
     https://github.com/esp8266/Arduino/issues/3451
  */
  http.setTimeout(HTTP_GLOBAL_TIMEOUT);

  //start http request with pin status url

  /*
     Add isFrom=esp in url
  */
  http.begin(PIN_STATUS_GET_URL);
  http.addHeader("Content-Type", "application/x-www-form-urlencoded");
  //get status of request and fail if not OK
  String payLoad;
  String postData = "device_mac=" + WiFi.macAddress() + "&no_of_schedules=4";

  int requestStatus = http.POST(postData);


  DynamicJsonBuffer buffer(JSON_PARSER_SIZE);


  if ( requestStatus == HTTP_CODE_OK )
    payLoad = http.getString();
  else
  {
    if (DEBUG) {
      Serial.println("request failed resetting");
      delay(1000);
      return;
//      ESP.reset();
    }
  }
  //  Serial.println(payLoad);
  /*
     temporarily removing actual string and replaing with stub string
  */
  //  payLoad="{\"changed\":\"true\",\"pin1\":\"1\"}";
  //  payLoad="{\"changed\":\"true\",\"pin1\":1,\"pin2\":0,\"pin3\":1,""}";
  //  payLoad = "{\"pin0\": {\"changed\": \"true\", \"status\":\"true\"},\"pin1\": {\"changed\": \"true\", \"status\":\"true\"}, \"pin2\": {\"changed\": \"true\", \"status\":\"true\"},\"pin3\": {\"changed\": \"true\", \"status\":\"true\"} }";
  //  payLoad="{}";
  //  payLoad = "{\"/pins\" : [{\"pin\": 0, \"state\": true, \"notify\": true},{\"pin\": 1, \"state\": true, \"notify\": false}, {\"pin\": 2, \"state\": false, \"notify\": true}, {\"pin\": 3, \"state\": false, \"notify\": false}]}";
  JsonObject& rootObject = buffer.parseObject(payLoad);
  if(!rootObject.success())
    return; // if parsing failed... return.
  DynamicJsonBuffer pinBuffer(20);
  for (int i = 0; i < 4; i++)
  {
    if (!Serial.available()) {
      JsonObject& pinObject=pinBuffer.createObject();
      if(rootObject["data"]["pins"][i]["notify_controller"].as<int>() == 0)
        continue;
      pinObject["pin_no"]= rootObject["data"]["pins"][i]["pin_no"];
      pinObject["pin_state"]=rootObject["data"]["pins"][i]["pin_state"];
      pinObject.printTo(Serial);
      Serial.println("\r");
      Serial.flush();
      resetNotify(pinObject["pin_no"].as<int>(),pinObject["pin_state"].as<int>());
//      delay(1000);
    }
  }

}

void postPinStatus(JsonObject& obj) {

  /*
     TODO: handle post data properly,
     url/content type = x-www-form-urlencoded/ pin=1&state=0&

     use state everywhere.

  */
//  resetNotify(obj["pin"].as<String>(),);
//  return;
  String payLoadToSend;
  payLoadToSend = "pin_no=" + obj["pin"].as<String>() + "&pin_state=" + obj["state"].as<String>() + "&device_mac=" + WiFi.macAddress()+"&notify_controller=0";
  /*
    obj.printTo(payLoadToSend);//
    if(DEBUG)
    Serial.println("Sending: "+payLoadToSend);
  */
  http.begin(PIN_STATUS_POST_URL);
  http.addHeader("Content-Type", "application/x-www-form-urlencoded");
  http.POST(payLoadToSend);
  
}
