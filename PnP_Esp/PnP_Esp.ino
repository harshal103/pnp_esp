#include "Config.h"
#include "Globals.h"
#include "PinServer.h"
#include "AutoConnect.h"
#include "PinStatus.h"

#include "ESP8266mDNS.h"

extern "C" {
#include "user_interface.h"
}


void setup() {
  // put your setup code here, to run once:
  Serial.begin(4800);

  // this doesn't work because esp does not have usb serial!
  while (!Serial);
  delay(2000);

  if (DEBUG) {
    Serial.println("Welcome to ESP!");
    Serial.print("My mac is: ");
    Serial.println(WiFi.macAddress());
  }
  //  WiFi.hostname("esp");
  // try to auto connect to wifi!
  autoConnect();

  
}

void loop() {
  /*
     if wifi connectin is lost in middle of something,
     try to auto connect and restart function loop!
  */
  if (WiFi.status() != WL_CONNECTED) {
    stopServer();
    autoConnect();
    return;
  }

  // request pin status from inernet!
  unsigned long time1 = millis();
  getPinStatus();
  if (DEBUG) {
    Serial.print("Request took :");
    Serial.print(millis() - time1);
    Serial.println(" ms");
  }

  // check if arduino has something to say
  if (Serial.available()) {


    String payLoadFromArduino = Serial.readString();

    if (DEBUG)
      Serial.printf("you said: %s\n", payLoadFromArduino.c_str());

    // if arduino says reset, then reset ESP!
    // does not work for now :(
    if (payLoadFromArduino.equals("reset\r\n")) {
      ESP.reset();
      delay(10000);
      return;
    }
    // for any other response, arduino is probably sending a json!
    else {
      // {"pin":"1","notify":false,"state":false} where X is number

      // try to parse json
      DynamicJsonBuffer buffer(JSON_PARSER_SIZE_SINGLE_PIN);
      JsonObject& fromArduino = buffer.parseObject(payLoadFromArduino);

      // if parsing failed, do nothing return
      if (!fromArduino.success()) {
        if (DEBUG)
          Serial.println("parsing json failed :(");
        return;
      }

      // otherwise putPinStatus to server.
      postPinStatus(fromArduino);
    }

  }
  if (DEBUG) {
    uint32_t free = system_get_free_heap_size();
    Serial.print("Available ram: ");
    Serial.println(free);

  }
  server.handleClient();
  //  ESP.reset();
}
