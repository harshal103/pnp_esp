
//Enable Debug mode globally
#define DEBUG false

#define SOFT_AP_SSID "PnP Auto 1.0"

//server url for pin status
#define PIN_STATUS_GET_URL "http://ersnexus.esy.es/p&psmartcontroller/controller/pandp_get_device_details.php"
#define PIN_STATUS_POST_URL "http://ersnexus.esy.es/p&psmartcontroller/controller/panp_update_pin_state_c.php"
//#define PIN_STATUS_URL "http://185.201.11.36/get_pnp_state_for_hardware.php"
//#define PIN_STATUS_URL "http://arduinojson.org/example.json"

//set http timeout to 15 seconds
#define HTTP_GLOBAL_TIMEOUT 15000
