//TODO: wifiManager.resetSettings(); on button press!

#include "ESP8266WiFi.h"
#include "DNSServer.h"
#include "ESP8266WebServer.h"
#include "WiFiManager.h"
#include "ESP8266mDNS.h"

void configModeCallback (WiFiManager *myWiFiManager) {
  if (DEBUG) {
    Serial.println("Entered config mode");
    Serial.println(WiFi.softAPIP());
    //if you used auto generated SSID, print it
    Serial.println(myWiFiManager->getConfigPortalSSID());
  }
}

void autoConnect() {
  //WiFiManager
  //Local intialization. Once its business is done, there is no need to keep it around

  WiFiManager wifiManager;
  //reset settings - for testing
  //wifiManager.resetSettings();

  //set callback that gets called when connecting to previous WiFi fails, and enters Access Point mode
  wifiManager.setAPCallback(configModeCallback);

  //set debug output off for wifimanager
  wifiManager.setDebugOutput(DEBUG);

  //fetches ssid and pass and tries to connect
  //if it does not connect it starts an access point with the specified name
  //here  "PnP Auto 1.0"
  //and goes into a blocking loop awaiting configuration
  if (!wifiManager.autoConnect(SOFT_AP_SSID)) {
    if (DEBUG)
      Serial.println("failed to connect and hit timeout");
    //reset and try again, or maybe put it to deep sleep
    ESP.reset();
    delay(1000);
  }

  //if you get here you have connected to the WiFi
  if (DEBUG) {
    Serial.println("connected :)");
    //    Serial.println(WiFi.localIP());
    Serial.printf("localIP: %s | gatewayIP: %s | dnsIP: %s |",
                  WiFi.localIP().toString().c_str(),
                  WiFi.gatewayIP().toString().c_str(),
                  WiFi.dnsIP().toString().c_str());
  }

  // start local server,
  setupServer();


  if (!MDNS.begin("esp") && DEBUG) {
    Serial.println("Error setting up MDNS responder!");
  }
  if (DEBUG)
    Serial.println("mDNS responder started");
  MDNS.addService("esp", "tcp", 80); // Announce esp tcp service on port 8080


}
