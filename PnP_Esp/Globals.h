#include "ESP8266HTTPClient.h"
#include <ESP8266WebServer.h>
#include "ArduinoJson.h"

HTTPClient http;
ESP8266WebServer server(80);

#define JSON_PARSER_SIZE JSON_ARRAY_SIZE(4) + JSON_OBJECT_SIZE(1) + 4*JSON_OBJECT_SIZE(3)+125
#define JSON_PARSER_SIZE_SINGLE_PIN JSON_OBJECT_SIZE(3)+29
